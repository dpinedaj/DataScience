#%%
#Carga de módulos y librerías
import pandas as pd 
pd.set_option('max_columns',7)
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler,PolynomialFeatures

#Filtrar alertas (futurewarnings)
from warnings import simplefilter
simplefilter(action = 'ignore', category = FutureWarning)

#%%
file_name='https://s3-api.us-geo.objectstorage.softlayer.net/cf-courses-data/CognitiveClass/DA0101EN/coursera/project/kc_house_data_NaN.csv'
df=pd.read_csv(file_name)

#%%
df.drop(['id','Unnamed: 0'],axis=1,inplace=True)
df.describe()

#%%
mean=df['bedrooms'].mean()
df['bedrooms'].replace(np.nan,mean, inplace=True)

#%%
mean=df['bathrooms'].mean()
df['bathrooms'].replace(np.nan,mean, inplace=True)

#%%
df['floors'].value_counts().to_frame()

#%%
sns.boxplot(x='waterfront',y='price',data=df)
plt.xlabel('Waterfront');plt.ylabel('Price')


#%%
sns.regplot('sqft_above','price',data=df)

#%%
df.corr()['price'].sort_values()

#%%
from sklearn.linear_model import LinearRegression


#%%
X = df[['long']]
Y = df['price']
lm = LinearRegression()
lm
lm.fit(X,Y)
lm.score(X, Y)

#%%
X=df[['sqft_living']]
y=df[['price']]
lr=LinearRegression()
lr.fit(X,y)
lr.score(X,y)

#%%
features =["floors", "waterfront","lat" ,"bedrooms" ,"sqft_basement" ,"view" ,"bathrooms","sqft_living15","sqft_above","grade","sqft_living"]     

#%%
X=df[features]
y=df[['price']]
lr=LinearRegression()
lr.fit(X,y)
lr.score(X,y)

#%%
features =["floors", "waterfront","lat" ,"bedrooms" ,"sqft_basement" ,"view" ,"bathrooms","sqft_living15","sqft_above","grade","sqft_living"]     

#%%
X=df[features];y=df[['price']]
#%%
Input=[('scale',StandardScaler()),('polynomial', PolynomialFeatures(include_bias=False)),('model',LinearRegression())]
pipe=Pipeline(Input)
pipe.fit(X,y)
pipe.score(X,y)



#algo
#%%
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import train_test_split
print("done")

#%%
features =["floors", "waterfront","lat" ,"bedrooms" ,"sqft_basement" ,"view" ,"bathrooms","sqft_living15","sqft_above","grade","sqft_living"]    
X = df[features ]
Y = df['price']

x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=0.15, random_state=1)


print("number of test samples :", x_test.shape[0])
print("number of training samples:",x_train.shape[0])

#%%
from sklearn.linear_model import Ridge

#%%
Rg=Ridge(alpha=0.1)
Rg.fit(x_train,y_train)
Rg.score(x_test,y_test)
#%%
pf=PolynomialFeatures(degree=2)
x_train=pf.fit_transform(x_train);x_test=pf.fit_transform(x_test)
rm=Ridge(alpha=0.1)
rm.fit(x_train,y_train)
rm.score(x_test,y_test)

#%%
