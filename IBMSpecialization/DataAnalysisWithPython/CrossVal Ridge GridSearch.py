
#%%
import pandas as pd
import numpy as np

# Import clean data 
path = 'https://s3-api.us-geo.objectstorage.softlayer.net/cf-courses-data/CognitiveClass/DA0101EN/module_5_auto.csv'
df = pd.read_csv(path)


#%%
df.to_csv('module_5_auto.csv')

#%% [markdown]
#  First lets only use numeric data 

#%%
df=df._get_numeric_data()
df.head()

#%% [markdown]
#  Libraries for plotting 

#%%
get_ipython().run_cell_magic('capture', '', '! pip install ipywidgets')


#%%
from IPython.display import display
from IPython.html import widgets 
from IPython.display import display
from ipywidgets import interact, interactive, fixed, interact_manual

#%% [markdown]
# <h2>Functions for plotting</h2>

#%%
def DistributionPlot(RedFunction, BlueFunction, RedName, BlueName, Title):
    width = 12
    height = 10
    plt.figure(figsize=(width, height))

    ax1 = sns.distplot(RedFunction, hist=False, color="r", label=RedName)
    ax2 = sns.distplot(BlueFunction, hist=False, color="b", label=BlueName, ax=ax1)

    plt.title(Title)
    plt.xlabel('Price (in dollars)')
    plt.ylabel('Proportion of Cars')

    plt.show()
    plt.close()


#%%
def PollyPlot(xtrain, xtest, y_train, y_test, lr,poly_transform):
    width = 12
    height = 10
    plt.figure(figsize=(width, height))
    
    
    #training data 
    #testing data 
    # lr:  linear regression object 
    #poly_transform:  polynomial transformation object 
 
    xmax=max([xtrain.values.max(), xtest.values.max()])

    xmin=min([xtrain.values.min(), xtest.values.min()])

    x=np.arange(xmin, xmax, 0.1)


    plt.plot(xtrain, y_train, 'ro', label='Training Data')
    plt.plot(xtest, y_test, 'go', label='Test Data')
    plt.plot(x, lr.predict(poly_transform.fit_transform(x.reshape(-1, 1))), label='Predicted Function')
    plt.ylim([-10000, 60000])
    plt.ylabel('Price')
    plt.legend()

#%% [markdown]
# <h1 id="ref1">Part 1: Training and Testing</h1>
# 
# <p>An important step in testing your model is to split your data into training and testing data. We will place the target data <b>price</b> in a separate dataframe <b>y</b>:</p>

#%%
y_data = df['price']

#%% [markdown]
# drop price data in x data

#%%
x_data=df.drop('price',axis=1)

#%% [markdown]
# Now we randomly split our data into training and testing data  using the function <b>train_test_split</b>. 

#%%
from sklearn.model_selection import train_test_split


x_train, x_test, y_train, y_test = train_test_split(x_data, y_data, test_size=0.15, random_state=1)


print("number of test samples :", x_test.shape[0])
print("number of training samples:",x_train.shape[0])

#%% [markdown]
# The <b>test_size</b> parameter sets the proportion of data that is split into the testing set. In the above, the testing set is set to 10% of the total dataset. 
#%% [markdown]
# <div class="alert alert-danger alertdanger" style="margin-top: 20px">
# <h1> Question  #1):</h1>
# 
# <b>Use the function "train_test_split" to split up the data set such that 40% of the data samples will be utilized for testing, set the parameter "random_state" equal to zero. The output of the function should be the following:  "x_train_1" , "x_test_1", "y_train_1" and  "y_test_1".</b>
# </div>

#%%
# Write your code below and press Shift+Enter to execute 
X_train_1,X_test_1,y_train_1,y_test_1=train_test_split(x_data,y_data,test_size=0.4,random_state=1)

#%%
from sklearn.linear_model import LinearRegression

#%% [markdown]
#  We create a Linear Regression object:

#%%
lre=LinearRegression()

#%% [markdown]
# we fit the model using the feature horsepower 

#%%
lre.fit(x_train[['horsepower']], y_train)

#%% [markdown]
# Let's Calculate the R^2 on the test data:

#%%
lre.score(x_test[['horsepower']], y_test)

#%% [markdown]
# we can see the R^2 is much smaller using the test data.

#%%
lre.score(x_train[['horsepower']], y_train)


#%%
# Write your code below and press Shift+Enter to execute 
x_train1, x_test1, y_train1, y_test1 = train_test_split(x_data, y_data, test_size=0.1, random_state=0)
lre.fit(x_train1[['horsepower']],y_train1)
lre.score(x_test1[['horsepower']],y_test1)



#%%
from sklearn.model_selection import cross_val_score

#%% [markdown]
# We input the object, the feature in this case ' horsepower', the target data (y_data). The parameter 'cv'  determines the number of folds; in this case 4. 

#%%
Rcross = cross_val_score(lre, x_data[['horsepower']], y_data, cv=4)
Rcross

#%% [markdown]
#  We can calculate the average and standard deviation of our estimate:

#%%
print("The mean of the folds are", Rcross.mean(), "and the standard deviation is" , Rcross.std())

#%% [markdown]
# We can use negative squared error as a score by setting the parameter  'scoring' metric to 'neg_mean_squared_error'. 

#%%
-1 * cross_val_score(lre,x_data[['horsepower']], y_data,cv=4,scoring='neg_mean_squared_error')

#%%
# Write your code below and press Shift+Enter to execute 
R2cross= cross_val_score(lre,x_data[['horsepower']],y_data,cv=2)
R2cross.mean()


#%%
from sklearn.model_selection import cross_val_predict

#%% [markdown]
# We input the object, the feature in this case <b>'horsepower'</b> , the target data <b>y_data</b>. The parameter 'cv' determines the number of folds; in this case 4. We can produce an output:

#%%
yhat = cross_val_predict(lre,x_data[['horsepower']], y_data,cv=4)
yhat[0:5]


#%%
lr = LinearRegression()
lr.fit(x_train[['horsepower', 'curb-weight', 'engine-size', 'highway-mpg']], y_train)

#%% [markdown]
# Prediction using training data:

#%%
yhat_train = lr.predict(x_train[['horsepower', 'curb-weight', 'engine-size', 'highway-mpg']])
yhat_train[0:5]

#%% [markdown]
# Prediction using test data: 

#%%
yhat_test = lr.predict(x_test[['horsepower', 'curb-weight', 'engine-size', 'highway-mpg']])
yhat_test[0:5]

#%% [markdown]
# Let's perform some model evaluation using our training and testing data separately. First  we import the seaborn and matplotlibb library for plotting.

#%%
import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')
import seaborn as sns

#%% [markdown]
# Let's examine the distribution of the predicted values of the training data.

#%%
Title = 'Distribution  Plot of  Predicted Value Using Training Data vs Training Data Distribution'
DistributionPlot(y_train, yhat_train, "Actual Values (Train)", "Predicted Values (Train)", Title)

#%%
Title='Distribution  Plot of  Predicted Value Using Test Data vs Data Distribution of Test Data'
DistributionPlot(y_test,yhat_test,"Actual Values (Test)","Predicted Values (Test)",Title)

#%% [markdown]
# Figur 2: Plot of predicted value using the test data compared to the test data. 
#%% [markdown]
# <p>Comparing Figure 1 and Figure 2; it is evident the distribution of the test data in Figure 1 is much better at fitting the data. This difference in Figure 2 is apparent where the ranges are from 5000 to 15 000. This is where the distribution shape is exceptionally different. Let's see if polynomial regression also exhibits a drop in the prediction accuracy when analysing the test dataset.</p>

#%%
from sklearn.preprocessing import PolynomialFeatures


#%%
x_train, x_test, y_train, y_test = train_test_split(x_data, y_data, test_size=0.45, random_state=0)

#%%
pr = PolynomialFeatures(degree=5)
x_train_pr = pr.fit_transform(x_train[['horsepower']])
x_test_pr = pr.fit_transform(x_test[['horsepower']])
pr

#%% [markdown]
# Now let's create a linear regression model "poly" and train it.

#%%
poly = LinearRegression()
poly.fit(x_train_pr, y_train)

#%%
yhat = poly.predict(x_test_pr)
yhat[0:5]

#%%
print("Predicted values:", yhat[0:4])
print("True values:", y_test[0:4].values)

#%% [markdown]
# We will use the function "PollyPlot" that we defined at the beginning of the lab to display the training data, testing data, and the predicted function.

#%%
PollyPlot(x_train[['horsepower']], x_test[['horsepower']], y_train, y_test, poly,pr)

#%%
poly.score(x_train_pr, y_train)

#%% [markdown]
#  R^2 of the test data:

#%%
poly.score(x_test_pr, y_test)

#%%
Rsqu_test = []

order = [1, 2, 3, 4]
for n in order:
    pr = PolynomialFeatures(degree=n)
    
    x_train_pr = pr.fit_transform(x_train[['horsepower']])
    
    x_test_pr = pr.fit_transform(x_test[['horsepower']])    
    
    lr.fit(x_train_pr, y_train)
    
    Rsqu_test.append(lr.score(x_test_pr, y_test))

plt.plot(order, Rsqu_test)
plt.xlabel('order')
plt.ylabel('R^2')
plt.title('R^2 Using Test Data')
plt.text(3, 0.75, 'Maximum R^2 ')    


#%%
def f(order, test_data):
    x_train, x_test, y_train, y_test = train_test_split(x_data, y_data, test_size=test_data, random_state=0)
    pr = PolynomialFeatures(degree=order)
    x_train_pr = pr.fit_transform(x_train[['horsepower']])
    x_test_pr = pr.fit_transform(x_test[['horsepower']])
    poly = LinearRegression()
    poly.fit(x_train_pr,y_train)
    PollyPlot(x_train[['horsepower']], x_test[['horsepower']], y_train,y_test, poly, pr)


#%%
interact(f, order=(0, 6, 1), test_data=(0.05, 0.95, 0.05))

#%%
pr=PolynomialFeatures(degree=2)
x_train_pr=pr.fit_transform(x_train[['horsepower', 'curb-weight', 'engine-size', 'highway-mpg','normalized-losses','symboling']])
x_test_pr=pr.fit_transform(x_test[['horsepower', 'curb-weight', 'engine-size', 'highway-mpg','normalized-losses','symboling']])

#%% [markdown]
#  Let's import  <b>Ridge</b>  from the module <b>linear models</b>.

#%%
from sklearn.linear_model import Ridge

#%% [markdown]
# Let's create a Ridge regression object, setting the regularization parameter to 0.1 

#%%
RigeModel=Ridge(alpha=0.1)

#%% [markdown]
# Like regular regression, you can fit the model using the method <b>fit</b>.

#%%
RigeModel.fit(x_train_pr, y_train)

#%% [markdown]
#  Similarly, you can obtain a prediction: 

#%%
yhat = RigeModel.predict(x_test_pr)

#%% [markdown]
# Let's compare the first five predicted samples to our test set 

#%%
print('predicted:', yhat[0:4])
print('test set :', y_test[0:4].values)

#%% [markdown]
# We select the value of Alfa that minimizes the test error, for example, we can use a for loop. 

#%%
Rsqu_test = []
Rsqu_train = []
dummy1 = []
ALFA = 10 * np.array(range(0,1000))
for alfa in ALFA:
    RigeModel = Ridge(alpha=alfa) 
    RigeModel.fit(x_train_pr, y_train)
    Rsqu_test.append(RigeModel.score(x_test_pr, y_test))
    Rsqu_train.append(RigeModel.score(x_train_pr, y_train))

#%% [markdown]
# We can plot out the value of R^2 for different Alphas 

#%%
width = 12
height = 10
plt.figure(figsize=(width, height))

plt.plot(ALFA,Rsqu_test, label='validation data  ')
plt.plot(ALFA,Rsqu_train, 'r', label='training Data ')
plt.xlabel('alpha')
plt.ylabel('R^2')
plt.legend()


#%%
# Write your code below and press Shift+Enter to execute 
RigeModel = Ridge(alpha=0) 
RigeModel.fit(x_train_pr, y_train)
RigeModel.score(x_test_pr, y_test)


#%%
from sklearn.model_selection import GridSearchCV

#%% [markdown]
# We create a dictionary of parameter values:

#%%
parameters1= [{'alpha': [0.001,0.1,1, 10, 100, 1000, 10000, 100000, 100000]}]
parameters1


#%%
RR=Ridge()
RR

#%%
Grid1 = GridSearchCV(RR, parameters1,cv=4)

#%% [markdown]
# Fit the model 

#%%
Grid1.fit(x_data[['horsepower', 'curb-weight', 'engine-size', 'highway-mpg']], y_data)

#%% [markdown]
# The object finds the best parameter values on the validation data. We can obtain the estimator with the best parameters and assign it to the variable BestRR as follows:

#%%
BestRR=Grid1.best_estimator_
BestRR

#%% [markdown]
#  We now test our model on the test data 

#%%
BestRR.score(x_test[['horsepower', 'curb-weight', 'engine-size', 'highway-mpg']], y_test)



#%%
# Write your code below and press Shift+Enter to execute 
parameters2= [{'alpha': [0.001,0.1,1, 10, 100, 1000,10000,100000,100000],'normalize':[True,False]} ]
Grid2 = GridSearchCV(Ridge(), parameters2,cv=4)
Grid2.fit(x_data[['horsepower', 'curb-weight', 'engine-size', 'highway-mpg']],y_data)
Grid2.best_estimator_
