

#%%Area Plot
df.plot(kind='area')
plt.title('titulo')
plt.ylabel('labely')
plt.xlabel('labelx')

#%%Histogram
df.plot(kind='hist')

#or using numpy
count,bin_edges = np.histogarm(df)
df.plot(kind='hist',xticks=bin_edges)
#%% Bar charts
df.plot(kind='bar')


#%% pie chart

df_grouped.plot(kind='pie')


#%% box plots
df.plot(kind='box')

#%% scatter
df.plot(
    kind='scatter',
    x='parameter1'
    y='parameter2'
)

#%%
