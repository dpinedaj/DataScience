#%%
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
#%%
df= pd.read_csv('Topic_Survey_Assignment.csv')

#%%

df
#%%
df.sort_values(by='Very interested',ascending=False,inplace=True)
#%%
df.set_index('Unnamed: 0',drop=True,inplace=True)
#%%
df

#%%
colors_list = ['#5cb85c','#5bc0de','#d9534f']

ax = (df.div(df.sum(1), axis=0)).plot(kind='bar',figsize=(15,4),width = 0.8,color = colors_list,edgecolor=None)
plt.legend(labels=df.columns,fontsize= 14)
plt.title("Percentage of Respondents' Interest in Data Science Areas",fontsize= 16)

plt.xticks(fontsize=14)
for spine in plt.gca().spines.values():
    spine.set_visible(False)
plt.yticks([])

for p in ax.patches:
    width, height = p.get_width(), p.get_height()
    x, y = p.get_xy() 
    ax.annotate('{:.0%}'.format(height), (x, y + height + 0.01))



#%%
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
#%%
df=pd.read_csv('Police_Department_Incidents_-_Previous_Year__2016_.csv')

#%%
df2=df[['PdDistrict','IncidntNum']].groupby(
    'PdDistrict',as_index=False
    ).count().sort_values(
        by='IncidntNum',ascending=False
        ).reset_index(
            drop=True).rename(
                columns={'PdDistrict':'Neighborhood','IncidntNum':'Count'})

#%%
df2

#%%
import folium



#%%
world_geo=r'san-francisco.geojson'
#%%
world_map=folium.Map(location=[37.8004,-122.402],zoom_start=12)
#%%
world_map.choropleth(
    geo_data=world_geo,
    data=df2,
    columns=['Neighborhood', 'Count'],
    key_on='feature.properties.DISTRICT',
    fill_color='YlOrRd', 
    fill_opacity=0.7, 
    line_opacity=0.2,
    legend_name='Crime on San Francisco'
)


#%%
world_map
#%%
