
import os
try:
	os.chdir(os.path.join(os.getcwd(), 'Data Visualization with Python'))
	print(os.getcwd())
except:
	pass
#%%
from IPython import get_ipython
#%%
import numpy as np  # useful for many scientific computing in Python
import pandas as pd # primary data structure library


#%%
df_can = pd.read_excel('https://s3-api.us-geo.objectstorage.softlayer.net/cf-courses-data/CognitiveClass/DV0101EN/labs/Data_Files/Canada.xlsx',
                       sheet_name='Canada by Citizenship',
                       skiprows=range(20),
                       skipfooter=2)

print ('Data read into a pandas dataframe!')
#%%
df_can.head()
#%%
df_can.tail()
#%%
df_can.info()
#%%
df_can.columns.values 

#%%
df_can.index.values

#%%
print(type(df_can.columns))
print(type(df_can.index))

#%%
df_can.columns.tolist()
df_can.index.tolist()
print (type(df_can.columns.tolist()))
print (type(df_can.index.tolist()))
#%%
# size of dataframe (rows, columns)
df_can.shape    
#%%
# in pandas axis=0 represents rows (default) and axis=1 represents columns.
df_can.drop(['AREA','REG','DEV','Type','Coverage'], axis=1, inplace=True)
df_can.head(2)
#%%
df_can.rename(columns={'OdName':'Country', 'AreaName':'Continent', 'RegName':'Region'}, inplace=True)
df_can.columns
#%%
df_can['Total'] = df_can.sum(axis=1)


#%%
df_can.isnull().sum()

#
#%%
df_can.describe()

#%%
df_can.Country  # returns a series
#%%
df_can[['Country', 1980, 1981, 1982, 1983, 1984, 1985]] # returns a dataframe
# notice that 'Country' is string, and the years are integers. 
# for the sake of consistency, we will convert all column names to string later on.

#%% [markdown]
# ### Select Row
# 
# There are main 3 ways to select rows:
# 
# ```python
#     df.loc[label]        
#         #filters by the labels of the index/column
#     df.iloc[index]       
#         #filters by the positions of the index/column
# ```

#%%
df_can.set_index('Country', inplace=True)
# tip: The opposite of set is reset. So to reset the index, we can use df_can.reset_index()
#%%
df_can.head(3)
#%%
# optional: to remove the name of the index
df_can.index.name = None

#%% [markdown]
# Example: Let's view the number of immigrants from Japan (row 87) for the following scenarios:
#     1. The full row data (all columns)
#     2. For year 2013
#     3. For years 1980 to 1985

#%%
# 1. the full row data (all columns)
print(df_can.loc['Japan'])

# alternate methods
print(df_can.iloc[87])
print(df_can[df_can.index == 'Japan'].T.squeeze())
#%%
# 2. for year 2013
print(df_can.loc['Japan', 2013])

# alternate method
print(df_can.iloc[87, 36]) # year 2013 is the last column, with a positional index of 36

#%%
# 3. for years 1980 to 1985
print(df_can.loc['Japan', [1980, 1981, 1982, 1983, 1984, 1984]])
print(df_can.iloc[87, [3, 4, 5, 6, 7, 8]])

#%% [markdown]
# Column names that are integers (such as the years) might introduce some confusion. For example, when we are referencing the year 2013, one might confuse that when the 2013th positional index. 
# 
# To avoid this ambuigity, let's convert the column names into strings: '1980' to '2013'.

#%%
df_can.columns = list(map(str, df_can.columns))
# [print (type(x)) for x in df_can.columns.values] #<-- uncomment to check type of column headers

#%% [markdown]
# Since we converted the years to string, let's declare a variable that will allow us to easily call upon the full range of years:

#%%
# useful for plotting later on
years = list(map(str, range(1980, 2014)))
years

#%% [markdown]
# ### Filtering based on a criteria
# To filter the dataframe based on a condition, we simply pass the condition as a boolean vector. 
# 
# For example, Let's filter the dataframe to show the data on Asian countries (AreaName = Asia).

#%%
# 1. create the condition boolean series
condition = df_can['Continent'] == 'Asia'
print(condition)


#%%
# 2. pass this condition into the dataFrame
df_can[condition]


#%%
# we can pass mutliple criteria in the same line. 
# let's filter for AreaNAme = Asia and RegName = Southern Asia

df_can[(df_can['Continent']=='Asia') & (df_can['Region']=='Southern Asia')]

# note: When using 'and' and 'or' operators, pandas requires we use '&' and '|' instead of 'and' and 'or'
# don't forget to enclose the two conditions in parentheses

#%% [markdown]
# Before we proceed: let's review the changes we have made to our dataframe.

#%%
print('data dimensions:', df_can.shape)
print(df_can.columns)
df_can.head(2)
#%%
# we are using the inline backend
get_ipython().run_line_magic('matplotlib', 'inline')

import matplotlib as mpl
import matplotlib.pyplot as plt

#%% [markdown]
# *optional: check if Matplotlib is loaded.

#%%
print ('Matplotlib version: ', mpl.__version__) # >= 2.0.0

#%% [markdown]
# *optional: apply a style to Matplotlib.

#%%
print(plt.style.available)
mpl.style.use(['ggplot']) # optional: for ggplot-like style

#%% [markdown]
# ### Plotting in *pandas*
# 
# Fortunately, pandas has a built-in implementation of Matplotlib that we can use. Plotting in *pandas* is as simple as appending a `.plot()` method to a series or dataframe.
# 
# Documentation:
# - [Plotting with Series](http://pandas.pydata.org/pandas-docs/stable/api.html#plotting)<br>
# - [Plotting with Dataframes](http://pandas.pydata.org/pandas-docs/stable/api.html#api-dataframe-plotting)
#%% [markdown]
# # Line Pots (Series/Dataframe) <a id="12"></a>
#%% [markdown]
# **What is a line plot and why use it?**
# 
# A line chart or line plot is a type of plot which displays information as a series of data points called 'markers' connected by straight line segments. It is a basic type of chart common in many fields.
# Use line plot when you have a continuous data set. These are best suited for trend-based visualizations of data over a period of time.
#%% [markdown]
# **Let's start with a case study:**
# 
# In 2010, Haiti suffered a catastrophic magnitude 7.0 earthquake. The quake caused widespread devastation and loss of life and aout three million people were affected by this natural disaster. As part of Canada's humanitarian effort, the Government of Canada stepped up its effort in accepting refugees from Haiti. We can quickly visualize this effort using a `Line` plot:
# 
# **Question:** Plot a line graph of immigration from Haiti using `df.plot()`.
# 
#%% [markdown]
# First, we will extract the data series for Haiti.

#%%
haiti = df_can.loc['Haiti', years] # passing in years 1980 - 2013 to exclude the 'total' column
haiti.head()

#%% [markdown]
# Next, we will plot a line plot by appending `.plot()` to the `haiti` dataframe.

#%%
haiti.plot()

#%% [markdown]
# *pandas* automatically populated the x-axis with the index values (years), and the y-axis with the column values (population). However, notice how the years were not displayed because they are of type *string*. Therefore, let's change the type of the index values to *integer* for plotting.
# 
# Also, let's label the x and y axis using `plt.title()`, `plt.ylabel()`, and `plt.xlabel()` as follows:

#%%
haiti.index = haiti.index.map(int) # let's change the index values of Haiti to type integer for plotting
haiti.plot(kind='line')

plt.title('Immigration from Haiti')
plt.ylabel('Number of immigrants')
plt.xlabel('Years')

plt.show() # need this line to show the updates made to the figure

#%% [markdown]
# We can clearly notice how number of immigrants from Haiti spiked up from 2010 as Canada stepped up its efforts to accept refugees from Haiti. Let's annotate this spike in the plot by using the `plt.text()` method.

#%%
haiti.plot(kind='line')

plt.title('Immigration from Haiti')
plt.ylabel('Number of Immigrants')
plt.xlabel('Years')

# annotate the 2010 Earthquake. 
# syntax: plt.text(x, y, label)
plt.text(2000, 6000, '2010 Earthquake') # see note below

plt.show() 


#%%
df_CI = df_CI.transpose()
df_CI.head()

