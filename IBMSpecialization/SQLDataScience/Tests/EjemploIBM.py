import sqlite3 as lite
#!--------------------------------CREAR/CONECTAR BASE DE DATOS------------------------------
miConexion = lite.connect("BBDD/PruebaIBM.db")
#!-------------------------CREAR CURSOR PARA HACER ACCIONES EN LA BBDD------------------------
miCursor = miConexion.cursor()
#!-------------------------------------CREAR TABLA E INGRESAR DATOS---------------------------
try:
  miCursor.execute("""CREATE TABLE INSTRUCTOR
  (ins_id INTEGER PRIMARY KEY NOT NULL,
  lastname VARCHAR(15) NOT NULL,
  firstname VARCHAR(15) NOT NULL,
  city VARCHAR(15),
  country CHAR(2))
  """)
except:
  miCursor.execute("DROP TABLE INSTRUCTOR")
  miCursor.execute("""CREATE TABLE INSTRUCTOR
  (ins_id INTEGER PRIMARY KEY NOT NULL,
  lastname VARCHAR(15) NOT NULL,
  firstname VARCHAR(15) NOT NULL,
  city VARCHAR(15),
  country CHAR(2))
  """)





miCursor.execute("""INSERT INTO INSTRUCTOR
  (ins_id, lastname, firstname, city, country)

  VALUES 
  (1, 'Ahuja', 'Rav', 'Toronto', 'CA')
  """)



miCursor.execute("""INSERT INTO INSTRUCTOR
  VALUES
  (2, 'Chong', 'Raul', 'Toronto', 'CA'),
  (3, 'Vasudevan', 'Hima', 'Chicago', 'US')
  """)


#!----------------------------------------------CONSULTAS---------------------------------------

for row in miCursor.execute("""SELECT * FROM INSTRUCTOR ORDER BY city desc """):
  print(row)


query = tuple(miCursor.execute("""SELECT firstname, lastname, country from INSTRUCTOR where city='Toronto' """))


print(len(query))


#!-------------------------------------------------MODIFICAR TABLA---------------------------------
miCursor.execute("""UPDATE INSTRUCTOR SET city='Markham' where ins_id=1 """)


query2 = tuple(miCursor.execute("""SELECT * FROM INSTRUCTOR """))
for i in query2:
  print(i)




miCursor.execute("""DELETE FROM INSTRUCTOR where ins_id=2 """)


miCursor.execute("""SELECT * FROM INSTRUCTOR """)

miConexion.commit()

miConexion.close()  



