#%%
import ibm_db

#%%
dsn_hostname = "dashdb-txn-sbox-yp-dal09-04.services.dal.bluemix.net" # e.g.: "hostname"
dsn_uid = "pdg89260"        # e.g. "YourDb2Username"
dsn_pwd = "vlzmzk49zh58x7^2"      # e.g. "YoueDb2Password"

dsn_driver = "{IBM DB2 ODBC DRIVER}"
dsn_database = "BLUDB"            # e.g. "BLUDB"    "db"
dsn_port = "50000"                # e.g. "50000"   port
dsn_protocol = "TCPIP"   

#%%
dsn = (
    "DRIVER={0};"
    "DATABASE={1};"
    "HOSTNAME={2};"
    "PORT={3};"
    "PROTOCOL={4};"
    "UID={5};"
    "PWD={6};").format(dsn_driver, dsn_database, dsn_hostname, dsn_port, dsn_protocol, dsn_uid, dsn_pwd)

#%%
try:
    conn = ibm_db.connect(dsn, "", "")
    print ("Connected to database: ", dsn_database, "as user: ", dsn_uid, "on host: ", dsn_hostname)

except:
    print ("Unable to connect: ", ibm_db.conn_errormsg() )

#%%
stmt = ibm_db.exec_immediate(conn,
"""CREATE TABLE Trucks(
    serial_no varchar(20) PRIMARY KEY NOT NULL,
    model VARCHAR(20) NOT NULL,
    manufacturer VARCHAR(20) NOT NULL,
    Engine_size VARCHAR(20) NOT NULL,
    Truck_Class VARCHAR(20) NOT NULL)"""
)

#%%
stmt = ibm_db.exec_immediate(conn,
"""INSERT INTO Trucks(serial_no, model, manufacturer, Engine_size, Truck_Class)
VALUES('A1234','Lonestar','International Trucks','Cummins ISX15','Class 8');"""
)
#%%
stmt = ibm_db.exec_immediate(conn,
"""INSERT INTO Trucks(serial_no, model, manufacturer, Engine_size, Truck_Class)
VALUES('B5432','Volvo VN','Volvo Trucks','Volvo D11','Heavy Duty Class 8');"""
)
stmt = ibm_db.exec_immediate(conn,
"""INSERT INTO Trucks(serial_no, model, manufacturer, Engine_size, Truck_Class)
VALUES('C5674','Kenworth W900','Kenworth Truck Co','Caterpillar C9','Class 8');"""
)

#%%
stmt = ibm_db.exec_immediate(conn, "SELECT * FROM Trucks")
ibm_db.fetch_both(stmt)

#%%
import pandas
import ibm_db_dbi
pconn = ibm_db_dbi.Connection(conn)
df = pandas.read_sql('SELECT * FROM Trucks', pconn)
df

#%%
