
#%%
import numpy as np 
import pandas as pd
from sklearn.tree import DecisionTreeClassifier

#%%
get_ipython().system('wget -O drug200.csv https://s3-api.us-geo.objectstorage.softlayer.net/cf-courses-data/CognitiveClass/ML0101ENv3/labs/drug200.csv')

#%%
my_data = pd.read_csv("drug200.csv", delimiter=",")
my_data[0:5]

#%%
# write your code here
my_data.shape

#%%
X = my_data[['Age', 'Sex', 'BP', 'Cholesterol', 'Na_to_K']].values
X[0:5]

#%% [markdown]
# As you may figure out, some features in this dataset are categorical such as __Sex__ or __BP__. Unfortunately, Sklearn Decision Trees do not handle categorical variables. But still we can convert these features to numerical values. __pandas.get_dummies()__
# Convert categorical variable into dummy/indicator variables.

#%%
from sklearn import preprocessing
le_sex = preprocessing.LabelEncoder()
le_sex.fit(['F','M'])
X[:,1] = le_sex.transform(X[:,1]) 


le_BP = preprocessing.LabelEncoder()
le_BP.fit([ 'LOW', 'NORMAL', 'HIGH'])
X[:,2] = le_BP.transform(X[:,2])


le_Chol = preprocessing.LabelEncoder()
le_Chol.fit([ 'NORMAL', 'HIGH'])
X[:,3] = le_Chol.transform(X[:,3]) 

X[0:5]

#%% [markdown]
# Now we can fill the target variable.

#%%
y = my_data["Drug"]
y[0:5]

#%%
from sklearn.model_selection import train_test_split

#%%
X_trainset, X_testset, y_trainset, y_testset = train_test_split(X, y, test_size=0.3, random_state=3)

#%%
# your code
print('X train shape: {} and Y train shape : {}'.format(X_trainset.shape,y_trainset.shape))


print('X test shape: {} and Y test shape : {}'.format(X_testset.shape,y_testset.shape))

#%%
drugTree = DecisionTreeClassifier(criterion="entropy", max_depth = 4)
drugTree # it shows the default parameters

#%%
drugTree.fit(X_trainset,y_trainset)

#%%
predTree = drugTree.predict(X_testset)

#%% [markdown]
# You can print out <b>predTree</b> and <b>y_testset</b> if you want to visually compare the prediction to the actual values.

#%%
print (predTree [0:5])
print (y_testset [0:5])


#%%
from sklearn import metrics
import matplotlib.pyplot as plt
print("DecisionTrees's Accuracy: ", metrics.accuracy_score(y_testset, predTree))


#%%
# Notice: You might need to uncomment and install the pydotplus and graphviz libraries if you have not installed these before
# !conda install -c conda-forge pydotplus -y
# !conda install -c conda-forge python-graphviz -y


#%%
from sklearn.externals.six import StringIO
import pydotplus
import matplotlib.image as mpimg
from sklearn import tree
get_ipython().run_line_magic('matplotlib', 'inline')


#%%
dot_data = StringIO()
filename = "drugtree.png"
featureNames = my_data.columns[0:5]
targetNames = my_data["Drug"].unique().tolist()
out=tree.export_graphviz(drugTree,feature_names=featureNames, out_file=dot_data, class_names= np.unique(y_trainset), filled=True,  special_characters=True,rotate=False)  
graph = pydotplus.graph_from_dot_data(dot_data.getvalue())  
graph.write_png(filename)
img = mpimg.imread(filename)
plt.figure(figsize=(100, 200))
plt.imshow(img,interpolation='nearest')
