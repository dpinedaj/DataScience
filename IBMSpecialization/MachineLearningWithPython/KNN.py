
#%% [markdown]
# **K-Nearest Neighbors** is an algorithm for supervised learning. Where the data is 'trained' with data points corresponding to their classification. Once a point is to be predicted, it takes into account the 'K' nearest points to it to determine it's classification.


#%%
import itertools
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import NullFormatter
import pandas as pd
import numpy as np
import matplotlib.ticker as ticker
from sklearn import preprocessing
get_ipython().run_line_magic('matplotlib', 'inline')

#%% [markdown]
# Lets download the dataset. To download the data, we will use !wget to download it from IBM Object Storage.

#%%
get_ipython().system('wget -O teleCust1000t.csv https://s3-api.us-geo.objectstorage.softlayer.net/cf-courses-data/CognitiveClass/ML0101ENv3/labs/teleCust1000t.csv')

#%%
df = pd.read_csv('teleCust1000t.csv')
df.head()


#%%
df.columns
#%%
X = df[['region', 'tenure','age', 'marital', 'address', 'income', 'ed', 'employ','retire', 'gender', 'reside']] .values  #.astype(float)
X[0:5]

#%%
y = df['custcat'].values
y[0:5]


#%%
X = preprocessing.StandardScaler().fit(X).transform(X.astype(float))
X[0:5]



#%%
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split( X, y, test_size=0.2, random_state=4)
print ('Train set:', X_train.shape,  y_train.shape)
print ('Test set:', X_test.shape,  y_test.shape)


#%%
from sklearn.neighbors import KNeighborsClassifier

#%%
k = 4
#Train Model and Predict  
neigh = KNeighborsClassifier(n_neighbors = k).fit(X_train,y_train)
neigh

#%% [markdown]
# ### Predicting
# we can use the model to predict the test set:

#%%
yhat = neigh.predict(X_test)
yhat[0:5]

#%% [markdown]
# ### Accuracy evaluation
# In multilabel classification, __accuracy classification score__ is a function that computes subset accuracy. This function is equal to the jaccard_similarity_score function. Essentially, it calculates how closely the actual labels and predicted labels are matched in the test set.

#%%
from sklearn import metrics
print("Train set Accuracy: ", metrics.accuracy_score(y_train, neigh.predict(X_train)))
print("Test set Accuracy: ", metrics.accuracy_score(y_test, yhat))

#%% [markdown]
# ## Practice
# Can you build the model again, but this time with k=6?

#%%
# write your code here
neight6 = KNeighborsClassifier(n_neighbors=6).fit(X_train,y_train)
yhat6=neight6.predict(X_test)
print("Train set Accuracy: ", metrics.accuracy_score(y_train, neight6.predict(X_train)))
print("Test set Accuracy: ", metrics.accuracy_score(y_test, yhat6))

#%% [markdown]
# Double-click __here__ for the solution.
# 
# <!-- Your answer is below:
#     
#     
# k = 6
# neigh6 = KNeighborsClassifier(n_neighbors = k).fit(X_train,y_train)
# yhat6 = neigh6.predict(X_test)
# print("Train set Accuracy: ", metrics.accuracy_score(y_train, neigh6.predict(X_train)))
# print("Test set Accuracy: ", metrics.accuracy_score(y_test, yhat6))
# 
# -->
#%% [markdown]
# #### What about other K?
# K in KNN, is the number of nearest neighbors to examine. It is supposed to be specified by the User. So, how can we choose right value for K?
# The general solution is to reserve a part of your data for testing the accuracy of the model. Then chose k =1, use the training part for modeling, and calculate the accuracy of prediction using all samples in your test set. Repeat this process, increasing the k, and see which k is the best for your model.
# 
# We can calculate the accuracy of KNN for different Ks.

#%%
Ks = 10
mean_acc = np.zeros((Ks-1))
std_acc = np.zeros((Ks-1))
ConfustionMx = [];
for n in range(1,Ks):
    
    #Train Model and Predict  
    neigh = KNeighborsClassifier(n_neighbors = n).fit(X_train,y_train)
    yhat=neigh.predict(X_test)
    mean_acc[n-1] = metrics.accuracy_score(y_test, yhat)

    
    std_acc[n-1]=np.std(yhat==y_test)/np.sqrt(yhat.shape[0])

mean_acc

#%% [markdown]
# #### Plot  model accuracy  for Different number of Neighbors 

#%%
plt.plot(range(1,Ks),mean_acc,'g')
plt.fill_between(range(1,Ks),mean_acc - 1 * std_acc,mean_acc + 1 * std_acc, alpha=0.10)
plt.legend(('Accuracy ', '+/- 3xstd'))
plt.ylabel('Accuracy ')
plt.xlabel('Number of Nabors (K)')
plt.tight_layout()
plt.show()


#%%
print( "The best accuracy was with", mean_acc.max(), "with k=", mean_acc.argmax()+1) 

