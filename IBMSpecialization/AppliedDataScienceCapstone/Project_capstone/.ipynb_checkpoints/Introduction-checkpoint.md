
## Predicting most expensive Boroughs in Singapore
>Daniel Pineda Jaramillo, November 2019

### 1.Introduction

1.1 Background
According to reports, Singapore is the city with the highest cost of living in the world, so it is 
interesting to investigate which are the main markets that increase this value, where they are located 
and with this, identify the boroughs with the highest cost of living


1.2 Problem

With the available data we can determine which stores and type of merchant are the most expensive and
represents a focus to search it. Including that kind of stores in my investigation with the foursquare API,
i can determine which of these stores are nearby to the center of each borough with some spended radius, and so
perform a heathmap for the cost of life.


1.3 Interest
For a person who decides to live in or visit Singapore, it is important to determine the most 
evaluated places with the highest cost of living.